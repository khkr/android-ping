package com.fsemicolon.androidping;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

public class MainActivity extends AppCompatActivity {

    Button btn_ping;
    EditText et_ip;
    private int CHOOSE = 0;
    TextView tv_show;
    String lost = "";
    String delay = "";
    String ip_adress = "";
    String countCmd = "";// ping -c
    String sizeCmd = "", timeCmd = "";// ping -s ;ping -i
    String result = "";
    private static final String tag = "TAG";
    int status = -1;
    String ping, ip, count, size, time;
    long delaytime = 0;
    // Myhandler handler=null;
    Handler handler1 = null;
    Thread a = null;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_ping = (Button) findViewById(R.id.btn_ping);
        et_ip = (EditText) findViewById(R.id.edit_ip);

        btn_ping.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub


                ip=et_ip.getText().toString();
                String countCmd = " -c " + "4" + " ";
                String sizeCmd = " -s " + "64" + " ";
                String timeCmd = " -i " + "1" + " ";
                String ip_adress = ip;
                ping = "ping" + countCmd + timeCmd + sizeCmd + ip_adress;


                tv_show = (TextView) findViewById(R.id.tv_show);

                delaytime = (long) Double.parseDouble(String.valueOf(1.0));
                Log.i(tag, "====MainThread====:" + Thread.currentThread().getId());


                handler1 = new Handler() {
                    public void handleMessage(Message msg)
                    {
                        switch (msg.what) {
                            case 10:
                                String resultmsg = (String) msg.obj;
                                tv_show.append(resultmsg);
                                Log.i(tag, "====handlerThread====:"
                                        + Thread.currentThread().getId());
                                Log.i(tag, "====resultmsg====:" + msg.what);
                                Log.i(tag, "====resultmsg====:" + resultmsg);
                                break;
                            default:
                                break;
                        }
                    }
                };

                a = new Thread()
                {
                    public void run() {
                       delay = "";
                        lost = "";

                        Process process = null;
                        BufferedReader successReader = null;
                        BufferedReader errorReader = null;

                        DataOutputStream dos = null;
                        try {
                            process = Runtime.getRuntime().exec(ping);
                            // dos = new DataOutputStream(process.getOutputStream());
                            Log.i(tag, "====receive====:");



                            // status = process.waitFor();
                            InputStream in = process.getInputStream();

                            OutputStream out = process.getOutputStream();
                            // success

                            successReader = new BufferedReader(
                                    new InputStreamReader(in));

                            // error
                            errorReader = new BufferedReader(new InputStreamReader(
                                    process.getErrorStream()));

                            String lineStr;

                            while ((lineStr = successReader.readLine()) != null) {

                                Log.i(tag, "====receive====:" + lineStr);
                                Message msg = handler1.obtainMessage();
                                msg.obj = lineStr + "\r\n";
                                msg.what = 10;
                                msg.sendToTarget();
                                result = result + lineStr + "\n";
                                if (lineStr.contains("packet loss")) {
                                    Log.i(tag, "=====Message=====" + lineStr.toString());
                                    int i = lineStr.indexOf("received");
                                    int j = lineStr.indexOf("%");
                                    Log.i(tag,
                                            "====packet-loss====:"
                                                    + lineStr.substring(i + 10, j + 1));//
                                    lost = lineStr.substring(i + 10, j + 1);
                                }
                                if (lineStr.contains("avg")) {
                                    int i = lineStr.indexOf("/", 20);
                                    int j = lineStr.indexOf(".", i);
                                    Log.i(tag,
                                            "====packet-avg:===="
                                                    + lineStr.substring(i + 1, j));
                                    delay = lineStr.substring(i + 1, j);
                                    delay = delay + "ms";
                                }
                                // getLocalMacAddress() + getGateWay());
                                sleep(delaytime * 1000);
                            }
                            // tv_show.setText(result);

                            while ((lineStr = errorReader.readLine()) != null) {
                                Log.i(tag, "==error======" + lineStr);
                                // tv_show.setText(lineStr);
                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            try {
                                if (dos != null) {
                                    dos.close();
                                }
                                if (successReader != null) {
                                    successReader.close();
                                }
                                if (errorReader != null) {
                                    errorReader.close();
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            if (process != null) {
                                process.destroy();
                            }
                        }
                    }
                };
                a.start();






            }
        });
    }
}